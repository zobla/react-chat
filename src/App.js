import React from "react";
import { Grid } from "semantic-ui-react";
import "./App.css";
import { connect } from "react-redux";
import Messages from "./components/Messages/Messages";
import SidePanel from "./components/SidePanel/SidePanel";

const App = props => (
  <Grid className="app" style={{ background: "#eee" }} columns="equal">
    <SidePanel key={props.currentUser && props.currentUser.id} currentUser={props.currentUser} />
    <Grid.Column style={{ marginLeft: "320px" }}>
      <Messages
        key={props.currentChannel && props.currentChannel.id}
        currentChannel={props.currentChannel}
        currentUser = {props.currentUser}
        isPrivateChannel = {props.isPrivateChannel}
      />
    </Grid.Column>
  </Grid> 
);

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
  currentChannel: state.channel.currentChannel,
  isPrivateChannel: state.channel.isPrivateChannel
});

export default connect(mapStateToProps)(App);
