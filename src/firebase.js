import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

var firebaseConfig = {
    apiKey: "AIzaSyAAbpEb3DW1U8dzI3FN5TbGHIgN96NKZoE",
    authDomain: "react-chat-app-30bfb.firebaseapp.com",
    databaseURL: "https://react-chat-app-30bfb.firebaseio.com",
    projectId: "react-chat-app-30bfb",
    storageBucket: "react-chat-app-30bfb.appspot.com",
    messagingSenderId: "543729375423",
    appId: "1:543729375423:web:d5fc9c3660880974"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);


export default firebase;