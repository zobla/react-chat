import React, { Component } from "react";
import { Grid, Header, Icon, Dropdown } from "semantic-ui-react";
import firebase from '../../firebase';

class UserPanel extends Component {
  state = {
      user: this.props.currentUser
  }
 
  dropDownOptions = () => [
      {
          key: 'user',
          text: <span>Signed is as <strong>{this.state.user.displayName}</strong></span>,
          disabled: true
      },
      {
          key: 'signout',
          text: <span onClick={this.handleSignout}>Sign out</span>
      }
  ]


  handleSignout = () => {
      firebase
      .auth().signOut()
      .then(() => console.log('Signed out'))
  }
  render() {
    return (
      <Grid style={{ background: "#41003b" }}>
        <Grid.Column>
          <Grid.Row style={{ padding: "1.2em", margin: 0 }}>
            <Header inverted floated="left" as="h2">
              <Icon name="rocketchat" />
              <Header.Content>Chat App</Header.Content>
            </Header>
          </Grid.Row>
          <Header style={{padding: '0.25em'}} as="h4" inverted>
              <Dropdown trigger={
                  <span>{this.state.user.displayName}</span>
              } options={this.dropDownOptions()}/>
          </Header>
        </Grid.Column>
      </Grid>
    );
  }
}

export default UserPanel;
